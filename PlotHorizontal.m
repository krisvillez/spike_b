function PlotHorizontal(level,plotstr,varargin)

% -------------------------------------------------------------------------
% Spike_B toolbox - PlotHorizontal.m
% -------------------------------------------------------------------------
% Description
%
% This function allows to plot a horizontal line in the current axes.
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	PlotHorizontal(level,plotstr,varargin)
%
% INPUT
%   level       :   Location of horizontal line along ordinate axis
%   plotstr     :   String specifying line properties (e.g., 'r--')
%   varargin    :   Additional entries for the plot function, given in
%                   pairs (e.g., ...,'LineWidth',3,... )
%
% OUTPUT
%   N/A
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-17
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2006-2015 Kris Villez
%
% This file is part of the Spike_B Toolbox for Matlab/Octave. 
% 
% The Spike_B Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_B Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<2 || isempty(plotstr)
    plotstr =   'k-'    ;
end

level           =   level(:)'   ;
nlevel          =   length(level);
ax              =   axis    ;
plot((ax(1:2)+[.000001 -.000001])'*ones(1,nlevel),ones(2,1)*level,plotstr,varargin{:})
	