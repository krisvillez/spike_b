function TF = UsingOctave

% -------------------------------------------------------------------------
% Spike_B toolbox - UsingOctave.m
% -------------------------------------------------------------------------
% Description
%
% This function evaluates whether Octave is the running platform
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	TF = UsingOctave() 
%
% INPUT
%   N/A
%
% OUTPUT
%   TF	:	Boolean indicating if Octave is being used as a platform.
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-04-16
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2006-2015 Kris Villez
%
% This file is part of the Spike_B Toolbox for Matlab/Octave. 
% 
% The Spike_B Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_B Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General 
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SCS Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

%
% This function implements the observability classification algorithm as
% provided by [1].
%
% Syntax: 	G = UsingOctave() 
%
%	Inputs: N/A
%
%	Outputs: 	
%		TF	Boolean indicator: True if code is run in Octave, false
%           otherwise
%
% [1] Kretsovalis, A. and Mah, R. S. H. (1987). Observability and
% redundancy classification in multicomponent process networks. AIChE
% Journal, 33(1), 70-82.  
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2013-10-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2013 Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


a = ver ;
TF =  strcmp(a(1).Name,'Octave')    ;