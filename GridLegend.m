function han = GridLegend(ph,str,nCol,initX,initY) 

nLine       =	length(str)         ;
nRow        =	ceil(nLine/nCol)	;
accX        =	0                   ;

%OBJ         =   nan(nRow,nCol,3)	;
RECT        =   nan(nCol,4)         ;

for iCol=1:nCol
    index       =	((iCol-1)*nRow+1):min(iCol*nRow,nLine)              ;
    [lh,objs]	=	legend(ph(index),str(index),'Interpreter','latex')  ;
    PosLeg      =	lh.Position                          ;
    PosAx       =	get(gca,'Position');
    PosLeg_rel  =	[	(PosLeg(1)-PosAx(1))/PosAx(3)	...
                        (PosLeg(2)-PosAx(2))/PosAx(4)	...
                        PosLeg(3)/PosAx(3)              ...
                        PosLeg(4)/PosAx(4)              ]   ;
    Xlim        =   get(gca,'Xlim')                         ;
    Ylim        =   get(gca,'Ylim')                         ;
    rect        =	[	PosLeg_rel(1)*(Xlim(2)-Xlim(1))+Xlim(1) ...
                        PosLeg_rel(2)*(Ylim(2)-Ylim(1))+Ylim(1) ...
                        PosLeg_rel(3)*(Xlim(2)-Xlim(1))         ...
	                    PosLeg_rel(4)*(Ylim(2)-Ylim(1))         ]   ;
    rect(1)     =	initX+accX      ;
    rect(2)     =	initY           ;
    accX        =	accX+rect(3)	;
    RECT(iCol,:)=	rect            ;
    
    for iRow=1:length(index)
        for iDepth = 1:2
            
            obj	=	objs(length(index)+(iRow-1)*2+iDepth)                   ;
            
            OBJ(iRow,iCol,iDepth).XData             =	obj.XData           ;
            OBJ(iRow,iCol,iDepth).YData             =	obj.YData           ;
            OBJ(iRow,iCol,iDepth).Color             =	obj.Color           ;
            OBJ(iRow,iCol,iDepth).LineStyle         =	obj.LineStyle       ;
            OBJ(iRow,iCol,iDepth).LineWidth         =	obj.LineWidth       ;
            OBJ(iRow,iCol,iDepth).Marker            =	obj.Marker          ;
            OBJ(iRow,iCol,iDepth).MarkerSize        =	obj.MarkerSize      ;
            OBJ(iRow,iCol,iDepth).MarkerFaceColor	=	obj.MarkerFaceColor ;
            
        end
        
        iDepth	=   3           ;
        obj     =	objs(iRow)	;
        
        OBJ(iRow,iCol,iDepth).Position              =	obj.Position            ;
        OBJ(iRow,iCol,iDepth).String                =	obj.String              ;
        OBJ(iRow,iCol,iDepth).Color                 =   obj.Color               ;
        OBJ(iRow,iCol,iDepth).FontName              =	obj.FontName            ;
        OBJ(iRow,iCol,iDepth).FontSize              =	obj.FontSize            ;
        OBJ(iRow,iCol,iDepth).FontWeight            =	obj.FontWeight          ;
        OBJ(iRow,iCol,iDepth).Interpreter           =   obj.Interpreter         ;
        OBJ(iRow,iCol,iDepth).HorizontalAlignment	=   obj.HorizontalAlignment ;
        OBJ(iRow,iCol,iDepth).VerticalAlignment     =   obj.VerticalAlignment	;
    end
    
    delete(lh)  ;
    
end
RECT(:,4)	=   max(RECT(:,4))  ;
RECT(:,2)   =   min(RECT(:,2))  ;

RECTMAX     =	RECT(1,:)       ;
RECTMAX(3)	=	accX            ;

rectangle('Position',RECTMAX,'edgecolor','k','linestyle','-')

for iCol=1:nCol
    
    rect	=	RECT(iCol,:)	;
    
    xstart	=	rect(1)         ;
    ystart	=	rect(2)         ;
    width	=	rect(3)         ;
    height	=	rect(4)         ;
    
    index	=	((iCol-1)*nRow+1):min(iCol*nRow,nLine)	;
    for iRow=1:length(index)
        
        for iDepth = 1:2
            xx	=	xstart+width * OBJ(iRow,iCol,iDepth).XData	;
            yy	=	ystart+height* OBJ(iRow,iCol,iDepth).YData	;
            han(iRow,iCol,iDepth)	=	plot (xx,yy,...
                'Color',            OBJ(iRow,iCol,iDepth).Color,...
                'LineStyle',        OBJ(iRow,iCol,iDepth).LineStyle,...
                'LineWidth',        OBJ(iRow,iCol,iDepth).LineWidth,...
                'Marker',           OBJ(iRow,iCol,iDepth).Marker,...
                'MarkerSize',       OBJ(iRow,iCol,iDepth).MarkerSize,...
                'MarkerFaceColor',  OBJ(iRow,iCol,iDepth).MarkerFaceColor);
            
        end
        
        iDepth	=   3   ;
        Pos     =	OBJ(iRow,iCol,iDepth).Position ;
        Pos     =	[	xstart+width*Pos(1)	ystart+height*Pos(2)	]   ;
        han(iRow,iCol,iDepth)	=	text ( ...
            Pos(1),Pos(2),          OBJ(iRow,iCol,iDepth).String,...
            'Color',                OBJ(iRow,iCol,iDepth).Color,...
            'FontName',             OBJ(iRow,iCol,iDepth).FontName,...
            'FontSize',             OBJ(iRow,iCol,iDepth).FontSize,...
            'FontWeight',           OBJ(iRow,iCol,iDepth).FontWeight,...
            'Interpreter',          OBJ(iRow,iCol,iDepth).Interpreter,...
            'HorizontalAlignment',  OBJ(iRow,iCol,iDepth).HorizontalAlignment,...
            'VerticalAlignment',	OBJ(iRow,iCol,iDepth).VerticalAlignment);
    end
end

end

